
(function ($) {

	$.extend(true, window, {
		adcgrid: grid
	});



	function grid(selector, cols, rows)
	{
		var initialized = false,
		container,
		rowTpl = $("<tr/>"),
		checkboxTpl = $("<input/>", {
			'type': 'checkbox'
		}),
		pagination = $('<div/>', {
			'class': 'paginator'
		}),
		perPage = 20,
		colHandlers = {},
		data = [],
		self = this


		function initialize()
		{
			container = $(selector)

			container.append(pagination)
			container.append('<table><thead class="grid_head"></thead><tbody class="grid_content"></tbody></table>');
			container.append(pagination)
			renderHead(cols);
			initialized = true;
		}


		/**
		 * Рендеринг шапки
		 * принимает массив объектов колонок
		 */
		function renderHead(cols)
		{
			$('.grid_head *', container).remove().parent().append('<tr></tr>');
			$('.grid_head', container).append('<tr></tr>')


			for (var i in cols)
			{

				if (_.has(cols[i], 'fields')){
					//парент ров
					var colspan = cols[i].fields.length;

					if (colspan > 0 && $('.grid_head tr', container).length == 1)
					{
						$('.grid_head', container).append('<tr></tr>')
					}


					if (_.has(cols[i], 'empty') && cols[i].empty)
					{
						$('.grid_head tr:eq(0)', container).append('<td colspan="'+colspan+'"></td>')
					}
					else
					{
						$('.grid_head tr:eq(0)', container).append('<td colspan="'+colspan+'">'+cols[i].name+'</td>')

					}


					for(var c in cols[i].fields)
					{
						var col = cols[i].fields[c]

						if (_.has(col, 'empty') && col.empty)
						{
							$('.grid_head tr:eq(1)', container).append('<td ' +  (_.has(col, 'class')? "class='"+col['class']+"'": "") +'></td>')
						}
						else
						{
							var cell = $("<td/>");

							$(cell, {'class': col.class || ''})

							//TODO: определять степень вложености
							if(_.has(col, 'checkbox'))
							{
								col.name = $('<input/>', { 'type': 'checkbox', 'class': 'selectAll'})
								$(col.name).bind("click", function(){
									if ($(this).prop('checked'))
										CheckAll()
									else
										UnCheckAll()
								})
							}

							$(cell).append(col.name)

							if(_.has(col, 'sortable'))
							{
								$(cell).bind('click', function(){
									console.log($(this))
									var dest = $(this).hasClass('asc')? 'desc': 'asc'
									Sort($(this).attr('data-sorter'), dest)
									$(".asc,.desc",$(this).parent()).removeClass('asc').removeClass('desc')
									$(this).addClass(dest)
								}).attr('data-sorter', col.field)
							}
							$('.grid_head tr:eq(1)', container).append(cell)

						}



						var cell = $("<td/>", {
							'class': col.field || (_.has(col, "checkbox")? "checkbox" : "")
						})

						$(rowTpl).append(cell)

						colHandlers[col.field] = _.has(col, 'modificator')? col.modificator: col.field

					}
				}
			}

			//проставить класс для всех td которые являются "родителями"
			if($('.grid_head tr').length > 1)
				$('.grid_head tr:eq(0) td').addClass('parent')
		}

		function CheckAll(){
			var content = $('.grid_content', container)

			$('input:checkbox', content).prop('checked', true)
		}

		function UnCheckAll(){
			var content = $('.grid_content', container)

			$('input:checkbox', content).prop('checked', false)
		}


		function Pagination(pages, page){
			pages = parseInt(pages);
			page = parseInt(page);
			var offset = 5,
			$p = $(pagination),
			from = 1,
			to = from + offset,
			first = false,
			last = true;

			//ebat-kolotit
			if(page > offset && (pages - page) >= offset){
				console.log(1)
				first = true
				from = (page - offset) + 1;
				to = (page + offset) - 1;
			} else if (page > offset && ((pages - page) <= offset)){
				console.log(2)
				first = true
				from = (page - offset) + 1;
				to = pages
				last = pages == page? false : true
			} else if (page < offset && pages > offset){
				console.log(3)
				to = ((offset + page) -1) > pages ? pages : ((offset + page) -1) ;
				last = true;
			} else if (page <= offset && pages <= offset){
				console.log(4)
				to = pages
				last = false
			}

			$("*", $p).remove();

			if  (pages <= 1)
				return;

			if (first)
			{
				var	$li = $("<li/>"),
				$a = $("<a/>")
				$a.attr('href','javascript: void(0)').html('&lsaquo;').bind('click', function(){
					self.show(1)
					} )
				$p.append($li.append($a))
			}



			for (var i = from; i<= to; i++)
			{
				var	$li = $("<li/>"),
				$a = $("<a/>")

				if( page == i )
					$li.addClass('current')

				$li.append($a.attr('href', 'javascript: void(0)').html(i).bind('click', function(){
					self.show($(this).html())
				}))

				$p.append($li)

			}

			if (last)
			{
				var	$li = $("<li/>"),
				$a = $("<a/>")
				$a.attr('href','javascript: void(0)').html('&rsaquo;').bind('click', function(){
					self.show(pages)
					} )
				$p.append($li.append($a))
			}
		}


		/**
		 * Магическим образом созраняет данные переданые в грид.
		 */
		function StoreData(rows){
			data = rows;
		}

		function getField(obj, fields){
			while (fields.length)
				obj = obj[fields.pop()]

			return obj;
		}


		function Show(page){
			page = parseInt(page) || 1;

			var content = $('.grid_content', container)
			//очищаем старый контент
			$('*', content).remove();


			var adsTo = page * perPage,
			adsFrom = adsTo - perPage;

			if(data == undefined){
				console.log('no data')
				return;
			}

			for (var i in data)
			{
				if (adsFrom <= i && i < adsTo)
				{
					var tpl = rowTpl.clone()

					$("td", tpl).each(function(){
						var field = $(this).attr('class'),
						xfields = field.split('.').reverse();


						if(field == 'checkbox'){
							$(this).append(checkboxTpl.clone())
							return
						}



						if (_.has(colHandlers, field) && typeof colHandlers[field] == 'function')
							$(this).append(colHandlers[field].apply(data[i]))
						else
						{
							if(xfields.length > 1)
								$(this).append(getField(data[i], xfields))
							else
								$(this).append(data[i][field])
						}

					})

					content.append(tpl)
				}
			}

			var pages = Math.ceil(data.length / perPage)
			Pagination(pages, page);

		}


		function uSort (criteria, destination)
		{
			destination = destination || 'asc'

			var xfields = criteria.split('.').reverse();

			var getProp = function(object)
			{
				var fields = xfields.slice(),
				comparedField = object;

				while(fields.length)
					comparedField = comparedField[fields.pop()];

				return comparedField;
			}

			return function(a, b){
				if (destination == 'asc')
					return (getProp(a) < getProp(b))? 1: (getProp(a) > getProp(b))? -1 : 0;
				else
					return (getProp(a) < getProp(b))? -1: (getProp(a) > getProp(b))? 1 : 0;
			};

		}

		function Sort(field, destination){
			console.log(field, destination)
			data.sort(uSort(field, destination))
			Show(1);
		}

		initialize();

		/**
		 * Public API
		 */

		this.render = {}

		this.render.head = function(cols){
			if (!cols)
				return false;

			renderHead(cols);
		}

		this.render.body = function(){
			alert('render body')
		}

		this.saveData = function(data){
			StoreData(AdsList)
		}


		this.getData = function(){
			return data
		}

		this.show = function(page){
			Show(page)
		}


		this.checkAll = function(){
			CheckAll()
		}

		this.uncheckAll = function(){
			UnCheckAll()
		}

		this.perPage = function(v){
			perPage = v || 20
			this.show(1)
		}

		this.sort = function(field, destination){
			Sort(field, destination)
		}


	}
}(jQuery));


