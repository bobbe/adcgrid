var ad = function(adObj){
	//удобоваримые поля
	// объект вида <исходное поле>: <поле в нашем классе>
	// либо значением может быть объект в котором должно присутсвовать поле field с именем поля
	// и поле 'cb' значением которого является функция, которая проделывает какие либо действия (например сконкатенировать поля таргетинга)
	// this будет принимать поле
	var fields = {
		"id": "id",
		'name': "name",
		'status': "status",
		'targetingView': {
			'field': 'targeting',
			'cb': function(){
				var targeting = ''
				for (var i in this)
					targeting += this[i] + ' '

				return targeting;
			}
		},
		'target_info': 'target_info',
		'cost_type': 'cost_type',
		'status': 'status',
		'stats': 'stats',
		'status_path': 'status_path',
		'cost': 'curBid',
		'mb': 'maxBid',
		'rec_bid': 'recBid',
		'layout': {
			'field': 'image',
			'cb': function(){
				for (var i in this)
					if(i == 'image_src')
						return this[i]
			}
		},
		'tactics': {
			'field': 'tactics',
			'cb': function(){
				var res = ''
				for(var i in this)
					res += this[i] + ' '
				return res
			}
		},
		'tt': 'timeTargeting'

	}


	for(var i in adObj){
		if (typeof i != 'undefined' &&  i in fields)
			if (typeof fields[i] == 'object')
				this[fields[i].field] = fields[i].cb.call(adObj[i])
			else
				this[fields[i]] = adObj[i]
	}

}



function uSort (criteria, destination)
{
		destination = destination || 'asc'

		var xfields = criteria.split('.').reverse();

		var getProp = function(object)
		{
			var fields = xfields.slice(),
				comparedField = object;

			while(fields.length)
				comparedField = comparedField[fields.pop()];

			return comparedField;
		}

		return function(a, b){
			if (destination == 'asc')
				return (getProp(a) < getProp(b))? 1: (getProp(a) > getProp(b))? -1 : 0;
			else
				return (getProp(a) < getProp(b))? -1: (getProp(a) > getProp(b))? 1 : 0;
		};

};



var AdsList = [],
	AdsListCopy = [];

var AdsControl = {
	perPage: 20,

	totalAds: 0,

	page: 1,

	pages: 1,

	pics: true,

	filter: false,

	dissabledFields: [],

	tpl: false,

	//tpl: _.template($("#rowTpl").html()),

	_picsInit: function(){
		if(getCookie('pics') == 'false')
		{
			AdsControl.pics = false
			$('#disablePics').attr('checked', false)
		} else
			$('#disablePics').attr('checked', true)


	},

	PicsHandler: function(){
		if($(this).attr('checked'))
		{
			setCookie('pics', true)
			AdsControl.pics = true;
		}
		else
		{
			setCookie('pics', false)
			AdsControl.pics = false;
		}



		AdsControl.Show(1)
	},

	SortingHandler: function(){

		$(".fixedHead tr .sorter-trigger").each(function(){
			$(this).click(function(){
				//TODO: destination
				var dest = 'asc';

				if ($(this).hasClass('asc'))
				{

					$(this).removeClass('asc').addClass('desc')
					dest = 'desc';
				} else {

					$(this).removeClass('desc').addClass('asc');
					dest = 'asc';
				}



				AdsControl.Sort($(this).attr('data-sorter'),dest )

			})
		})
	},
	ColsFilterHandler: function(){
		var colsFilter = $(".cols-filter")

		AdsControl.dissabledFields = $.parseJSON(getCookie('Ads:DissabledFields'))

		if(AdsControl.dissabledFields != null)
			for (var i in AdsControl.dissabledFields)
				$("[data-item="+AdsControl.dissabledFields[i]+"]", colsFilter).addClass('disactive')
		else
			AdsControl.dissabledFields = []

		colsFilter.show()



		$('div', colsFilter).each(function(){
			$(this).click(function(){
				var item = $(this)
				if ($(item).hasClass('disactive'))
				{
					AdsControl.dissabledFields = _.without(AdsControl.dissabledFields, $(item).attr('data-item'));
					$(item).removeClass('disactive')
				}
				else
				{
					AdsControl.dissabledFields.push($(item).attr('data-item'))
					$(item).addClass('disactive')
				}
				//save cookie
				setCookie('Ads:DissabledFields', $.toJSON(AdsControl.dissabledFields))


				AdsControl.Show(1)
			})
		})


	},

	Initialize: function(){
		AdsControl._picsInit()

		AdsControl.tpl = _.template($("#rowTpl").html())

	    AdsControl.Fetch();

		$("#disablePics").change(AdsControl.PicsHandler);
		AdsControl.SortingHandler();
		AdsControl.ColsFilterHandler();

		ColsFilter.Init();

		$(".showAdsOnPage").change(AdsControl.ShowAdsOnPageHandler);

		$("#adsFilter").change(function(){
			AdsControl.StatusFilterHandler($(this).val())
		})

		TargetingFilter.initialize();
	},

	Fetch: function(camp, to, from){
		camp = camp || $("#campaign").val()
		ok.rpc('Ads:FetchAdsObj', {
			campaign: camp,
			to: to,
			from: from,
			sid: AdCenter.sessionID
		}, function(resp){
			AdCenter.On('\\VKBatch\\AdsGetAdsStatistics', function(){
				if(AdCenter.sessionID == this.sid)
				{
					this.SetChecked();
					AdsControl.Append(this.result)
				}
			})
		})
	},

	Append: function(statistics){
//console.log(statistics)
		UI.Progress.Hide();

		if(statistics == null || (_.isArray(statistics) && statistics.length == 0)){
			$(".prel").hide();
			var $noAds = '<tr><td colspan="21" class="noAds">Рекламные объявления отсутствуют. <a href="/ad/new">Создать ?</a></td></tr>'
			$("#adsTable tbody").append($noAds);
			return;
		}

		AdsControl.perPage = Storage.getItem('AdsListPerPage')? Storage.getItem('AdsListPerPage'): 20

		AdsControl.totalAds = statistics.length

		AdsControl.pages = Math.ceil(AdsControl.totalAds/AdsControl.perPage);

		AdsControl.Pagination(this.pages, this.page);

		AdsList = []

		for(var i in statistics)
		{
			var s = statistics[i]
//			if(s.tactics.length != 0)
//				console.log(s)

			//filter targeting params for table view
			var newTargeting = [];

			if(_.has(s, 'targetting') && _.has(s.targetting, 'country') && s.targetting.country != 0)
				newTargeting.push(s.target_info['country'][0])
			else
				newTargeting.push('Любая')

			if(_.has(s.targetting, 'cities'))
				for(var cid in s.target_info.city)
					newTargeting.push(s.target_info.city[cid])
			else
				newTargeting.push('')

			if(s.targetting.sex == 1)
				newTargeting.push('Ж.')
			else if (s.targetting.sex == 2)
				newTargeting.push('М.')
			else
			   newTargeting.push('М.Ж.')

			s.targetingView = newTargeting

			AdsList.push(new ad(s))
		}

		AdsControl.Show(1);

		//сперва очищаем футер, это нужно для корректного отображения статистики за период
		$("#adsTable tfoot *").remove()

		AdsControl.FooterHandler();
	},

	Pagination: function(pages, page){
		pages = parseInt(pages);
		page = parseInt(page);
		var offset = 5,
		$p = $(".paginator"),
		from = 1,
		to = from + offset,
		first = false,
		last = true;

		//ebat-kolotit
		if(page > offset && (pages - page) >= offset){
			console.log(1)
			first = true
			from = (page - offset) + 1;
			to = (page + offset) - 1;
		} else if (page > offset && ((pages - page) <= offset)){
			console.log(2)
			first = true
			from = (page - offset) + 1;
			to = pages
			last = pages == page? false : true
		} else if (page < offset && pages > offset){
			console.log(3)
			to = ((offset + page) -1) > pages ? pages : ((offset + page) -1) ;
			last = true;
		} else if (page <= offset && pages <= offset){
			console.log(4)
			to = pages
			last = false
		}

		$("*", $p).remove();



		if(pages <= 1)
			return;

		if(first){
			var	$li = $("<li/>"),
			$a = $("<a/>")
			$a.attr('href','javascript: void(0)').html('&lsaquo;').bind('click', function(){   AdsControl.Show(1)} )
			$p.append($li.append($a))
		}



		for(var i = from; i<= to; i++){
			var	$li = $("<li/>"),
			$a = $("<a/>")

			if( page == i )
				$li.addClass('current')

			$li.append($a.attr('href', 'javascript: void(0)').html(i).bind('click', function(){   AdsControl.Show($(this).html()) }))

			$p.append($li)

		}

		if(last){
			var	$li = $("<li/>"),
			$a = $("<a/>")
			$a.attr('href','javascript: void(0)').html('&rsaquo;').bind('click', function(){   AdsControl.Show(AdsControl.pages)} )
			$p.append($li.append($a))
		}

	},

	Show: function(page){
		console.time('Рендеринг объявлений')
//		console.profile('renderAds')

		$(".prel").hide();

		var container = $("#adsTable tbody")

		$("*", container).remove();


		var adsTo = page * this.perPage,
			adsFrom = adsTo - this.perPage;

		this.page = page;

		if(this.perPage != 0)
			this.pages= Math.ceil(AdsList.length / this.perPage)
		else
			this.pages = 0;


		this.Pagination(this.pages, this.page)

		Ad.UpdateCounters();



		for(var i in AdsList){
			var ad = AdsList[i];

			if (adsFrom <= i && i < adsTo)
			{


//				var row = AdsControl.tpl({
//								  'name': ad.name,
//								  'status': ad.status,
//								  'imageSrc': ad.image,
//								  'adID': ad.id,
//								  'xadtype': (ad.cost_type == 0)? 'cpc': 'cpm',
//								  'targeting': ad.targeting,
//								  'statusPathImg': ad.status_path,
//								  'impressions': ad.stats.impressions,
//								  'clicks': ad.stats.clicks,
//								  'ctr': ad.stats.ctr,
//								  'lead': ad.stats.join_rate,
//								  'cnv': ad.stats.converse,
//								  'spent': Payments.Conv(ad.stats.spent),
//								  'ecpm': Payments.Conv(ad.stats.ecpm),
//								  'ecpc': Payments.Conv(ad.stats.ecpc),
//								  'cpa': Payments.Conv(ad.stats.cpa),
//								  'curBid': Payments.Conv(ad.curBid),
//								  'maxBid': Payments.Conv(ad.maxBid),
//								  'recBid': ad.recBid? Payments.Conv(ad.recBid) : '',
//								  'timeTargeting': ad.timeTargeting == 1 ? '/tpl/ADC/img/watch_16x14.png': false,
//								  'tactics': ad.tactics
//								})

				var row = AdsControl.tpl({
								  'name': ad.name,
								  'status': ad.status,
								  'imageSrc': ad.image,
								  'adID': ad.id,
								  'xadtype': (ad.cost_type == 0)? 'cpc': 'cpm',
								  'targeting': ad.targeting,
								  'statusPathImg': ad.status_path,
								  'impressions': ad.stats.impressions,
								  'clicks': ad.stats.clicks,
								  'ctr': ad.stats.ctr,
								  'lead': ad.stats.join_rate,
								  'cnv': ad.stats.converse,
								  'spent': ad.stats.spent,
								  'ecpm': ad.stats.ecpm,
								  'ecpc': ad.stats.ecpc,
								  'cpa': ad.stats.cpa,
								  'curBid': ad.curBid,
								  'maxBid': ad.maxBid,
								  'recBid': ad.recBid? ad.recBid : '',
								  'timeTargeting': ad.timeTargeting == 1 ? '/tpl/ADC/img/watch_16x14.png': false,
								  'tactics': ad.tactics
								})
				container.append(row)
				//console.log(row)

//				var row = $("<tr />"),
//				col = $("<td />")
//				row.append($("<td />", {'style': 'width: 38px;', 'class': 'col-left woutPadding'}).append($("<input />", {
//					'type': 'checkbox',
//					'status': ad.status,
//					'name': 'ids[]',
//					'class': 'checkbox',
//					'value': ad.id,
//					'xadtype': (ad.cost_type == 0)? 'cpc': 'cpm',
//					'x-ad-id': ad.id,
//					'x-ad-name': ad.name,
//					'x-ad-type': (ad.cost_type == 0)? 'cpc': 'cpm'
//
//				} )))
//				row.append($("<td />", {
//					'style': 'width: 91px;'
//				}).append(function(){
//					var ret = $("<a/>", {
//								'text': AdsList[i].name,
//								'href': '/ad/' + AdsList[i].id
//								})
//
//					if (AdsControl.pics)
//					{
//
//						ret.append($("<img />", {'src':  AdsList[i].image}))
//					}
//					return ret
//				}
//				))
//
//
//
//				if(_.indexOf(AdsControl.dissabledFields, 'targeting') == -1)
//					row.append($("<td />", {
//						text: AdsList[i].targeting
//					}))
//
//				row.append($("<td />").append($("<img />", {'src': AdsList[i].status_path})))
//
//				row.append($("<td />"))
//				//statistics
//				row.append($("<td />", {
//					text: AdsList[i].stats.impressions
//				}))
//
//				row.append($("<td />", {
//					text: AdsList[i].stats.clicks
//				}))
//
//				row.append($("<td />", {
//					text: AdsList[i].stats.ctr
//				}))
//
//				//лиды
//				if(_.indexOf(AdsControl.dissabledFields, 'join_rate') == -1)
//					row.append($("<td />", {
//						text: AdsList[i].stats.join_rate
//					}))
//
//				if(_.indexOf(AdsControl.dissabledFields, 'cnv') == -1)
//					row.append($("<td />", {
//						text: AdsList[i].stats.converse
//					}))
//
//				row.append($("<td />", {
//					text: Payments.Conv(AdsList[i].stats.spent)
//				}))
//
//				if(_.indexOf(AdsControl.dissabledFields, 'ecpm') == -1)
//					row.append($("<td />", {
//						text: Payments.Conv(AdsList[i].stats.ecpm)
//					}))
//
//				if(_.indexOf(AdsControl.dissabledFields, 'ecpc') == -1)
//				row.append($("<td />", {
//					text: Payments.Conv(AdsList[i].stats.ecpc)
//				}))
//
//				row.append($("<td />", {
//					text: Payments.Conv(AdsList[i].stats.cpa)
//				}))
//
//				row.append($("<td />"))
//
//				//bids
//				if(_.indexOf(AdsControl.dissabledFields, 'bids') == -1)
//				{
//					row.append($("<td />", {
//						text: Payments.Conv(AdsList[i].curBid)
//					}))
//
//					row.append($("<td />", {
//						text: Payments.Conv(AdsList[i].maxBid)
//					}))
//
//					row.append($("<td />", {
//						text: AdsList[i].recBid? Payments.Conv(AdsList[i].recBid) : ''
//					}))
//				}
//
//				// end bids
//
//				row.append($("<td />"))
//
//
//				row.append($("<td />").append(function(){
//					if(AdsList[i].timeTargeting == 1)
//						return $('<img />', {'src': '/tpl/ADC/img/watch_16x14.png'})
//				}))
//
//				row.append($("<td />", {
//					text: AdsList[i].tactics
//				}))
//
//				container.append(row)
			}
			else
				continue;

		}


//		console.profileEnd('renderAds');
		console.timeEnd('Рендеринг объявлений')
		this.InitFixedHeader();
	},
	//обработчик селекта для показа объявлений на странице
	ShowAdsOnPageHandler: function(){

		var el = $('.showAdsOnPage');

		if(el.val() == 'all'){
			AdsControl.perPage = AdsList.length
		} else {
			AdsControl.perPage = parseInt(el.val())
		}

		AdsControl.Show(1);
	},

	FooterHandler: function(){
		var total = {
			impressions: 0,
			clicks: 0,
			ctr: 0,
			join_rate: 0,
			converse: 0,
			spent: 0,
			ecpm: false,
			ecpc: false,
			cpa: 0
		}

		for(var i in AdsList)
		{
			var ad = AdsList[i]
			for(var a in total){
				if(total[a]!==false)
					total[a] += parseFloat(ad.stats[a])
			}


		}

		if(total.impressions !== 0){
			total.ctr = parseFloat((total.clicks / total.impressions) * 100).toFixed(3) + '%'
		}



		total.spent = Payments.Curr(total.spent)

		total.impressions = RankDivider(total.impressions)
		total.clicks = RankDivider(total.clicks)


		var footer = $("#adsTable tfoot")


		$(footer).append('<tr class="tabFooter"></tr>')

		for(var i = 0; i < 5; i++)
			$('tr', footer).append("<td></td>")

		//добавление параметров
		for(var a in total){
			$('tr', footer).append(
				$("<td />", {
					'text': total[a] !==false? total[a]: '',
					'class': 'footVisible'
				})
			)
		}

		//console.log(total)
	},

	//позаимствовано из PromoInit.js и переписано
	InitFixedHeader: function()
	{
		console.time('Простановка размеров')
		if(!this.cache)
			this.cache = {};

		var winHeight = $(window).height()-279,
			colWidth = [];

		$('.adsFixedBody').css('height', winHeight);


		if(_.has(this.cache, 'size') && this.cache.size == winHeight)
			colWidth = this.cache.colWidth

		else {
			this.cache.size = winHeight
			$(".fixedHead th:visible").each(function(){
				colWidth.push($(this).css('width'));
			});

			this.cache.colWidth = colWidth
		}


		$("#adsTable tr").each(function(){
			for(var i in colWidth)
				$('td', $(this)).eq(i).attr('style','width: ' + colWidth[i])
		});


		console.timeEnd('Простановка размеров')
	},

	Sort: function(field, destination){
		AdsList.sort(uSort(field, destination))
		AdsControl.Show(1);
	},

	StatusFilterHandler: function(status){
		if(!this.filter && status != 'all')
			window.AdsCopy = AdsList;
		else if (status == 'all') {

			AdsList = AdsCopy
			AdsCopy = [];
			this.filter = false

			AdsControl.ShowAdsOnPageHandler()

			AdsControl.Show(1);
			return true;
		}

		this.filter = true;

		AdsList = [];

		for(var i in AdsCopy)
		{
			if(AdsCopy[i].status == status){
				AdsList.push(AdsCopy[i])
			}
		}

		AdsControl.ShowAdsOnPageHandler()

		AdsControl.Show(1);
	}

}
$(AdsControl.Initialize)