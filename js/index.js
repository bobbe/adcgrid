//common functions
function getCookie(name) {

	var matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));

	return matches ? decodeURIComponent(matches[1]) : undefined

}

function setCookie(name, value, expires){
    if (!expires) expires = 365
    var expiration = new Date();
    expiration.setDate( expiration.getDate() + expires );
    document.cookie = [ name+'=', value, '; expires=', expiration.toUTCString() ,';path=/' ].join( '' );
}




var columns = [
	{
		'name': 'Информация',
		'fields': [
			{
				//'name': '<input type="checkbox" class="selectAll" />',
				'checkbox': true
			},

			{
                'name': 'Объявление',
                'field': 'name',
                'sortable': true,
                'empty': false,
                'class': 'name',
				'modificator': function(){

					return "<a href='"+this.id+"'>"+this.name+"<img src='"+this.image+"' /></a>"
				}
			},

			{
				'name': 'Таргетинг',
//				'modificator': function(){
//						this.targeting.join();
//						return this.join(' ')
//				},
				'field': 'targeting',
				'class': 'targeting',
				'sortable': false
			},
			{
				'name': 'Статус',
				'field': 'status',
				'class': 'status'
			}
		]
	},

	{
		'empty': true,
		'class': 'separator',
		'fields': [{
			'empty': true,
			'class': 'separator'
		}]
	},

	{
		'name': 'Статистика',
		'fields': [
			{
				'name': 'Показы',
				'field': 'stats.impressions'
			},
			{
				'name': 'Переходы',
				'field': 'stats.clicks'
			},
			{
				'name': 'CTR',
				'field': 'stats.ctr'
			},
			{
				'name': 'Лиды',
				'field': 'stats.join_rate'
			},
			{
				'name': 'CNV',
				'field': 'stats.converse'
			},
			{
				'name': 'Потрачено',
				'field': 'stats.spent'
			},
			{
				'name': 'eCPM',
				'field': 'stats.ecpm'
			},
			{
				'name': 'eCPC',
				'field': 'stats.ecpc'
			},
			{
				'name': 'CPA',
				'field': 'stats.cpa'
			},
		]
	},

	{
		'empty': true,
		'class': 'separator',
		'fields': [{
			'empty': true,
			'class': 'separator'
		}]
	},

	{
		'name': 'Ставка',
		'fields': [
			{
				'name': 'Текущая',
				'field': 'curBid',
				'sortable': true
			},
			{
				'name': 'Макс.',
				'field': 'maxBid'
			},
			{
				'name': 'Рекоменд.',
				'field': 'recBid'
			}
		]
	},

	{
		'empty': true,
		'class': 'separator',
		'fields': [{
			'empty': true,
			'class': 'separator'
		}]
	},

	{
		'name': 'Робот',
		'fields': [
			{
				'name': 'ВТ',
				'field': 'timeTargeting'
			},
			{
				'name': 'Тактика',
				'field': 'tactics'
			}
		]
	}

];



//tmp
var ad = function(adObj){
	//удобоваримые поля
	// объект вида <исходное поле>: <поле в нашем классе>
	// либо значением может быть объект в котором должно присутсвовать поле field с именем поля
	// и поле 'cb' значением которого является функция, которая проделывает какие либо действия (например сконкатенировать поля таргетинга)
	// this будет принимать поле
	var fields = {
		"id": "id",
		'name': "name",
		'status': "status",
		'targetingView': {
			'field': 'targeting',
			'cb': function(){
				var targeting = ''
				for (var i in this)
					targeting += this[i] + ' '

				return targeting;
			}
		},
		'target_info': 'target_info',
		'cost_type': 'cost_type',
		'status': 'status',
		'stats': 'stats',

		'status_path': 'status_path',
		'cost': 'curBid',
		'mb': 'maxBid',
		'rec_bid': 'recBid',
		'layout': {
			'field': 'image',
			'cb': function(){
				for (var i in this)
					if(i == 'image_src')
						return this[i]
			}
		},
		'tactics': {
			'field': 'tactics',
			'cb': function(){
				var res = ''
				for(var i in this)
					res += this[i] + ' '
				return res
			}
		},
		'tt': 'timeTargeting'

	}


	for(var i in adObj){
		if (typeof i != 'undefined' &&  i in fields)
			if (typeof fields[i] == 'object')
				this[fields[i].field] = fields[i].cb.call(adObj[i])
			else
				this[fields[i]] = adObj[i]
	}

}
var AdsList = [];
window.data = JSON.parse(window.data)
for(var i in window.data)
{
	var s = window.data[i]


	//filter targeting params for table view
	var newTargeting = [];

	if(_.has(s, 'targetting') && _.has(s.targetting, 'country') && s.targetting.country != 0)
		newTargeting.push(s.target_info['country'][0])
	else
		newTargeting.push('Любая')

	if(_.has(s.targetting, 'cities'))
		for(var cid in s.target_info.city)
			newTargeting.push(s.target_info.city[cid])
	else
		newTargeting.push('')

	if(s.targetting.sex == 1)
		newTargeting.push('Ж.')
	else if (s.targetting.sex == 2)
		newTargeting.push('М.')
	else
	   newTargeting.push('М.Ж.')

	s.targetingView = newTargeting

	//имя + картинка для таблицы
	//весьма костыльно
	//s.name = "<a href='"+s.id+"'>"+s.name+"<img src='"+s.layout.image_src+"' /></a>"

	AdsList.push(new ad(s))
}


console.time('Grid')
var grid = new adcgrid('.grid', columns);
grid.saveData(AdsList)
grid.show(1);
console.timeEnd('Grid')
